# Robot Suiveur de Couleur

**Réalisation d'un robot suiveur de couleur avec une camera Pixy,  une carte Arduino, d'un capteur d'obstacles ultrasons et une plateforme motorisée.**

Dans le cadre de l'API **Dipfablab Avancé A18**.

**Membres du groupe** : Sarah, Véronique, Yasmina, Justin

## Attention !

- Il est nécessaire d'apprendre à la Pixy à reconnaître un objet/couleur/code couleur à l'aide du programme fourni par le constructeur.

- Ne pas utiliser de Motor Shield Arduino et la camera Pixy simultanément sur une
carte Arduino UNO car il y a une incompatibilité sur les ports utilisés.
Préférer l'utilisation d'un duo Arduino LEONARDO + Motor Shield.

- Préférer une salle dans laquelle la luminosité est constante pour l'utilisation de la Pixy (pas de contre-jour, etc.)

## Pour améliorer le système

- [x] Ajouter un capteur d'obstacles
- [x] Programmer la camera pour détecter un code couleur et non une simple couleur
- [ ] Utiliser une caméra plus performante (plage dynamique, résolution,... )
- [ ] Piloter la camera Pixy avec un servomoteur pour détecter l'objet derrière le robot
- [ ] Utiliser une technologie permettant la marche arrière des moteurs en cas de détection d'obstacle (Motor Shield, Pont en H, etc.)
- [ ] Passer sous Raspberry Pi avec un programme OpenCV pour une plus grande puissance